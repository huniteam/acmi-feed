# ACMI Feed

Docker container to present the ACMI data at https://github.com/ACMILabs/collection as a HuNI simple feed at https://acmi.huni.net.au

## Build

`docker-compose build`

## Test

`docker-compose up`

curl http://localhost:3000/resources.xml

## Push

`docker-compose push`

The new image will be automatically pulled before the daily feed update.
