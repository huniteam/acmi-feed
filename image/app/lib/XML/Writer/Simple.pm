package XML::Writer::Simple;

use 5.20.0;
use warnings FATAL => 'all';

use Function::Parameters    qw( :strict );
use Moo;

has fh => (
    is => 'ro',
    required => 1,
);

has _tag_stack => (
    is => 'ro',
    default => sub { [ ] },
);

method BUILD(@args) {
    binmode($self->fh, ":utf8");
    say {$self->fh} '<?xml version="1.0" encoding="utf-8"?>';
}

method indent() {
    return '  ' x (scalar @{ $self->_tag_stack });
}

method open($tag, $attr = {}) {
    say {$self->fh} $self->indent,  _format_tag($tag, $attr);
    push(@{ $self->_tag_stack }, $tag);
}

method close() {
    my $tag = pop(@{ $self->_tag_stack });
    say {$self->fh} $self->indent,  _format_tag("/$tag", {});
}

method element($tag, $content, $attr = {}) {
    say {$self->fh} $self->indent,
                    _format_tag($tag, $attr),
                    _escape($content),
                    _format_tag("/$tag", {});
}

fun _escape($text) {
	$text =~ s/&/&amp;/g;
	$text =~ s/"/&quot;/g;
	$text =~ s/'/&apos;/g;
	$text =~ s/</&lt;/g;
	$text =~ s/>/&gt;/g;

    return $text;
}

fun _format_tag($tag, $attr) {
    my $xml = "<$tag";
    if (%$attr) {
        $xml = join(' ', $xml,
            map { sprintf('%s="%s"', $_, _escape($attr->{$_})) }
            grep { defined $attr->{$_} && $attr->{$_} ne '' }
                sort keys %$attr);
    }
    $xml .= '>';
    return $xml;
}

1;
