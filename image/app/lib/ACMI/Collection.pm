package ACMI::Collection;

use 5.20.0;
use warnings;

use ACMI::Collection::Normaliser    qw( );
use ACMI::Collection::XMLWriter     qw( );
use Cpanel::JSON::XS                qw( decode_json );
use Digest::MD5                     qw( md5_hex );
use Function::Parameters            qw( :strict );
use Log::Any                        qw( $log );
use Moo;
use Path::Tiny                      qw( path );
use XML::Writer::Simple             qw( );

has records => (
    is => 'ro',
    default => sub { [ ] },
);

has system_id => (
    is => 'ro',
    default => sub { { } },
);

method convert($in, $out) {
    $self->load_records(path($in));
    $self->normalise_records;
    $self->save_records(path($out));
}

method load_records($dir) {
    $log->info('Loading JSON records.');
    my $iter = path($dir)->iterator({ recurse => 1 });
    my @records;
    while (my $file = $iter->()) {
        next unless $file =~ /\.json$/;
        push(@records, $self->load_record($file));
    }
    @{ $self->records } = sort { $a->{system_id} <=> $b->{system_id} } @records;

    my $count = scalar @records;
    $log->info("Loaded $count JSON records.");
}

method load_record($file) {
    my $record = decode_json(path($file)->slurp_utf8);

    my $system_id = $record->{system_id};
    my $acmi_id   = $record->{acmi_identifier};

    $self->system_id->{$acmi_id} = $system_id;

    return $record;
}

method normalise_records() {
    my $count = scalar @{ $self->records };
    $log->info("Normalising $count records.");
    my $normaliser =
        ACMI::Collection::Normaliser->new(system_id => $self->system_id);
    for my $record (@{ $self->records }) {
        $normaliser->normalise($record);
    }
}

method save_records($dir) {
    my $count = scalar @{ $self->records };
    $log->info("Saving $count records to XML.");

    my $xml_writer = ACMI::Collection::XMLWriter->new;
    my $resource_file = $dir->child('resources.xml');
    my $resources = XML::Writer::Simple->new(fh => $resource_file->openw);

    $resources->open(resources => { recordCount => $count });
    for my $record (@{ $self->records }) {

        my $id = $record->{system_id};
        my $type = lc $record->{record_type};
        $type =~ s/\s.*$//;
        my $filename = "acmi-${type}-${id}.xml";

        # Note that $xml is utf-8 encoded bytes.
        my $xml = $xml_writer->convert($record);
        $dir->child($filename)->spew_raw($xml);

        $resources->open('record');
        $resources->element(name => $filename);
        $resources->element(hash => md5_hex($xml));
        $resources->close;
    }
    $resources->close;

    $log->info("Saved $count records.");
}

1;
