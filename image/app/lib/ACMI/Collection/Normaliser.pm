package ACMI::Collection::Normaliser;

use 5.20.0;
use warnings;

use Data::Dumper::Concise   qw( Dumper );
use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );
use Moo;

has system_id => (
    is => 'ro',
    required => 1,
);

method normalise($record) {
    $self->strip_empty_keys($record);
    $self->normalise_creators($record);
    $self->normalise_languages($record);
    $self->normalise_members($record);
    $self->normalise_relations($record);

    return $record;
}

method strip_empty_keys($record) {
    for my $key (keys %$record) {
        my $value = $record->{$key};
        if ((ref $value eq 'ARRAY') && (@$value == 1) && ($value->[0] eq '')) {
            delete $record->{$key};
        }
    }

    return $record;
}

# Convert parallel array fields like:
#   other_languages
#   other_language_type
#   other_language_notes
#
# Into a single array of objects like:
#   {
#       other_language => ...,
#       type => ...,
#       notes => ...,
#   }
#
# In this case:
#   $primary_key = 'other_languages'
#   $attributes => {
#       other_languages      => 'other_language',
#       other_language_type  => 'type',
#       other_language_notes => 'note',
#   }
#
method merge_fields($record, $primary_key, $output_key, $attributes) {
    # If this record doesn't contain the primary key
    return unless exists $record->{$primary_key};

    # The attribute arrays may be short or missing - the primary key array
    # determines the final array size.
    my $count = scalar @{ $record->{$primary_key} };

    my %array;
    for my $record_key (keys %$attributes) {
        my $attr_key = $attributes->{$record_key};
        $array{$attr_key} = delete $record->{$record_key} // [];
    }

    my @merged;
    for my $index (0 .. $count-1) {
        my %field;
        for my $attr (keys %array) {
            $field{$attr} = $array{$attr}->[$index];
        }
        push(@merged, \%field);
    }

    $record->{$output_key} = \@merged;
}

method normalise_creators($record) {
    $self->merge_fields($record, creator_contributor_role =>
        creator_contributor => {
            creator_contributor_role => 'content',
        });

    for my $field (@{ $record->{creator_contributor} // [] }) {
        my $value = $field->{content};

        if ($value =~ /^(.*),\s*(.*)$/) {
            $field->{content} = $1;
            $field->{role} = $2;
        }
    }
}

method normalise_languages($record) {
    $self->merge_fields($record, other_languages => other_language => {
        other_languages      => 'content',
        other_language_type  => 'type',
        other_language_notes => 'note',
    });
}

method normalise_members($record) {
    $self->merge_fields($record, member_object => member_object => {
        member_object => 'content',
    });
    for my $relation (@{ $record->{member_object} // [] }) {
        $self->annotate_relation($relation);
    }
}

method normalise_relations($record) {
    $self->merge_fields($record, related_objects => related_object => {
        related_objects      => 'content',
        object_relationship  => 'relationship',
        related_object_notes => 'note'
    });
    for my $relation (@{ $record->{related_object} // [] }) {
        $self->annotate_relation($relation);
    }
}

method annotate_relation($relation) {
    if (my ($acmi_id) = ($relation->{content} =~ /^.*?;\s*(.*?);/)) {
        if (exists $self->system_id->{$acmi_id}) {
            $relation->{system_id} = $self->system_id->{$acmi_id};
        }
    }

    return $relation;
}

1;
