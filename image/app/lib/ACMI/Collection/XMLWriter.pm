package ACMI::Collection::XMLWriter;

use 5.20.0;
use warnings;

use Data::Dumper::Concise   qw( Dumper );
use Function::Parameters    qw( :strict );
use Log::Any                qw( $log );
use Moo;
use XML::Writer::Simple     qw( );

method convert($record) {
    my $xml;
    open(my $fh, '>:encoding(UTF-8)', \$xml)
        or die "Opening string filehandle: $!";

    my $writer = XML::Writer::Simple->new(fh => $fh);
    $writer->open('acmi');
    for my $key (sort keys %$record) {
        my $values = $record->{$key};
        if (ref $values ne 'ARRAY') {
            $values = [ $values ];
        }
        $self->convert_field($writer, $key, $_) for @$values;
    }
    $writer->close;

    close $fh;
    return $xml;
}

method convert_field($writer, $key, $value) {
    if (ref $value eq 'HASH') {
        my $content = delete $value->{content};
        $writer->element($key, $content, $value);
    }
    elsif ($value ne '') {
        $writer->element($key, $value);
    }
}

1;
