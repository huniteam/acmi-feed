#!/bin/sh -e

echo Fetching...
( cd $SRCDIR && git pull && git rev-parse HEAD > $DESTDIR/version )

echo Converting...
$BASEDIR/bin/convert-collection $SRCDIR $DESTDIR

echo Serving...
exec nginx -g 'daemon off;'
